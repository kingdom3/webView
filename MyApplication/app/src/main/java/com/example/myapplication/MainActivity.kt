package com.example.myapplication

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.webview)
        WebView.setWebContentsDebuggingEnabled(true)
        val myWebView: WebView = findViewById(R.id.webview)
        myWebView.loadUrl("https://www.naver.com")
        myWebView.apply {
            webViewClient = WebViewClient()
            settings.javaScriptEnabled = true
        }
    }
}